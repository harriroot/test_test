﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Mvc;
using HomeApp.Models;
using MongoDB.Bson.Serialization;
using Mues.Managers;

namespace HomeApp.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult Index()
        {
            return View();
        }

        //Vista del login
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(Usuarios usuario)
        {
            string nameDB = "hommunity";
            MongoConnection db = new MongoConnection(nameDB);
            string loginUsuario = db.GetLogin(usuario.Email, usuario.Password);
            //Si el usuario es correcto
            if (loginUsuario != string.Empty)
            {
                var usuarioResultante = BsonSerializer.Deserialize<Usuarios>(loginUsuario);
                Session["Usuario"] = usuarioResultante.Nombre;
                Session["Email"] = usuarioResultante.Email;
                //return Content("Logeado como: " +  Session["Usuario"]);
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ModelState.AddModelError("","Email o Password incorrectos");
            }
            return View();
        }

        //Cerrar sesion
        public ActionResult LogOut()
        {
            Session.Remove("Usuario");
            Session.Remove("Email");
            return RedirectToAction("Login", "Account");
        }
    }
}
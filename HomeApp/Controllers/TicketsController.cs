﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Mues.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Net.Http;
using MongoDB.Bson.Serialization;

namespace Angular.Controllers
{
    public class TicketsController : ApiController
    {
        public class TicketModel
        {
            [BsonRepresentation(BsonType.ObjectId)]
            public ObjectId id { get; set; }
            //public string id { get; }
            public string mensaje { get; set; }
            public int numero { get; set; }
        }

        static string nameDB = "hommunity";
        static string collection = "tickets";
        MongoConnection db = new MongoConnection(nameDB);

        [HttpGet()]
        [ActionName("GetTickets")]
        public IEnumerable<TicketModel> GetTickets()
        {
            var ticketResource = BsonSerializer.Deserialize<IEnumerable<TicketModel>>(db.GetCollection(collection));
            return ticketResource;
        }

        [HttpDelete()]
        [ActionName("DelTickets")]
        public IEnumerable<TicketModel> DelTickets(string id)
        {
            db.Remove(collection, id);
            return GetTickets();
        }
        
        [HttpPost()]
        [ActionName("InputTicket")]
        public IEnumerable<TicketModel> InputTicket(TicketModel ticket)
        {
            db.Insert(collection, ticket);
            return GetTickets();
        }

        [HttpGet()]
        [ActionName("GetDocument")]
        public IEnumerable<TicketModel> GetDocument(string id)
        {
             var ticket = BsonSerializer.Deserialize<IEnumerable<TicketModel>>(db.GetDocument(collection, id));
            return ticket;
        }

        [HttpPut()]
        [ActionName("UpdateTicket")]
        public IEnumerable<TicketModel> UpdateTicket(TicketModel ticket)
        {
            db.Update(collection, ticket);
            return GetTickets();
        }


    }
}
  
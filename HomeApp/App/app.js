﻿(function () {
    'use strict';

    angular.module('app', [
        'ui.router',
        'app.ticket',
        'app.condominos',
        'app.addcondomino',
        'app.detailcondomino'
    ])

        .service('Api', ['$http', ApiService])

        .config(function ($stateProvider, $urlRouterProvider, $httpProvider) {


            $stateProvider
                .state('ticket', {
                    url: '/ticket',
                    templateUrl: 'App/ticket/ticket.html',
                    controller: 'TicketCtrl'
                })
            $stateProvider
                .state('condominos', {
                    url: '/condomino',
                    templateUrl: 'App/condominos/condominos.html',
                    controller: 'CondominoCtrl'
                })
            $stateProvider
                .state('agregar', {
                    url: '/condomino/agregar',
                    templateUrl: 'App/condominos/agregar.html',
                    controller: 'AddCondominoCtrl'
                })
            $stateProvider
                .state('detalle', {
                    url: '/condomino/detalle',
                    templateUrl: 'App/condominos/detalle.html',
                    controller: 'DetCondominoCtrl'
            })

            $urlRouterProvider.otherwise('/');
        })

        .run(function ($rootScope, $timeout) {
            $rootScope.errorMessage === "";
            $rootScope.isError = false;

            $rootScope.setError = function (errorMessage) {
                $rootScope.errorMessage = errorMessage;
                if (errorMessage !== null && errorMessage !== "") {
                    $rootScope.isError = true;
                } else {
                    $rootScope.isError = false;
                }
                $timeout(function () {
                    $rootScope.setError(null);
                }, 3000);
            }
        })

        .controller('MainCtrl', ['$scope', function ($scope) {
            var me = $scope;
            me.hola = 'AngularJS';
        }])
})();
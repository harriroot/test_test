﻿(function () {
    'use strict';

    angular.module('app.ticket', [])

        .controller('TicketCtrl', ['$scope', 'Api', function ($scope, Api) {
            var me = $scope;
            me.models = {};
            me.modelo = {};
            me.document = {};

            me.consultar = function (id) {
                Api.GetApiCall("Tickets", id, function (event) {
                    if (event.hasErrors === true) {
                        //console.log('dentro del error');
                        me.setError(event.error);
                    } else {
                        // console.log('dentro del success ' + event);

                        //var des = window.JSON.parse(event.result);
                        //me.modelo = window.JSON.parse(event.result);
                        //alert( window.JSON.stringify(eval("(" + event.result + ")")));
                        console.log(event.result);
                    }
                })
            }

            me.GetTickets = function () {
                Api.GetApiCall("Tickets", "GetTickets", function (event) {
                    if (event.hasErrors === true) {
                        //console.log('dentro del error');
                        me.setError(event.error);
                    } else {
                       // console.log('dentro del success ' + event);
                        me.models = event.result;
                    }
                });
            }

            me.remover = function (id, numero) {
                alert('¿Remover mensaje '+numero+'?')
                    Api.DeleteApiCall("Tickets", id, function (event) {
                        if (event.hasErrors === true) {
                            //console.log('dentro del error');
                            me.setError(event.error);
                        } else {
                            //console.log('dentro del success ' + event);
                            me.models = event.result;
                        }
                    });
            }

            me.AgregarTicket = function () {
               
                //console.log(me.modelo);

                Api.PostApiCall("Tickets", "InputTicket", me.modelo, function (event) {
                    if (event.hasErrors === true) {
                        //console.log('dentro del error');
                        me.setError(event.error);
                    } else {
                        // console.log('dentro del success ' + event);
                        me.models = event.result;
                    }
                });
                me.modelo = {};


            }

            me.editar = function (id)
            {
                alert(id);
                
            }
        }])
})();

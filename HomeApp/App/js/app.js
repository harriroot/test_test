(function () {
    'use strict';

    var brandPrimary = '#36a9e1';
    var brandSuccess = '#bdea74';
    var brandInfo = '#67c2ef';
    var brandWarning = '#fabb3d';
    var brandDanger = '#ff5454';

    var grayDark = '#34383c';
    var gray = '#9fabb8';
    var grayLight = '#dbdee0';
    var grayLighter = '#e9ebec';
    var grayLightest = '#f9f9f9';

    angular.module('app', [
        'ui.router',
        'oc.lazyLoad',
        'pascalprecht.translate',
        'ncy-angular-breadcrumb',
        'angular-loading-bar',
        'ngSanitize',
        'ngAnimate',
        'app.ticket',
        'app.condominos',
        'app.addcondomino',
        'app.detailcondomino',
        'app.ingresos',
        'app.addingresos',
        'app.egresos',
        'app.addegresos',
        'app.detegresos',
        'app.proveedores',
        'app.addproveedor',
        'app.detproveedor',
        'app.configuracion',
        'app.conceptos',
        'app.propiedades',
        'app.perfil'
    ])
        //Template DashboardProps
        .config(['cfpLoadingBarProvider', function (cfpLoadingBarProvider) {
            cfpLoadingBarProvider.includeSpinner = false;
            cfpLoadingBarProvider.latencyThreshold = 1;
        }])
        .run(['$rootScope', '$state', '$stateParams', function ($rootScope, $state, $stateParams) {
            $rootScope.$on('$stateChangeSuccess', function () {
                document.body.scrollTop = document.documentElement.scrollTop = 0;
            })
            $rootScope.$state = $state;
            return $rootScope.$stateParams = $stateParams;
        }])


        .service('Api', ['$http', ApiService])

        .config(function ($stateProvider, $urlRouterProvider, $httpProvider) {

            
            $stateProvider
                .state('agregar', {
                    url: '/condomino/agregar',
                    templateUrl: 'App/condominos/agregar.html',
                    controller: 'AddCondominoCtrl'
                })
            $stateProvider
                .state('detalle', {
                    url: '/condomino/detalle',
                    templateUrl: 'App/condominos/detalle.html',
                    controller: 'DetCondominoCtrl'
                })

            $urlRouterProvider.otherwise('/');
        })

        .run(function ($rootScope, $timeout) {
            $rootScope.errorMessage === "";
            $rootScope.isError = false;

            $rootScope.setError = function (errorMessage) {
                $rootScope.errorMessage = errorMessage;
                if (errorMessage !== null && errorMessage !== "") {
                    $rootScope.isError = true;
                } else {
                    $rootScope.isError = false;
                }
                $timeout(function () {
                    $rootScope.setError(null);
                }, 3000);
            }
        })

        .controller('MainCtrl', ['$scope', function ($scope) {
            var me = $scope;
            me.hola = 'AngularJS';
        }])
})();
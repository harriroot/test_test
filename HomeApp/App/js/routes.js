angular
.module('app')
.config(['$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider', '$breadcrumbProvider', function($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, $breadcrumbProvider) {

  $urlRouterProvider.otherwise('/dashboard');

  $ocLazyLoadProvider.config({
    // Set to true if you want to see what and when is dynamically loaded
    debug: true
  });

  $breadcrumbProvider.setOptions({
    prefixStateName: 'app.main',
    includeAbstract: true,
    template: '<li class="breadcrumb-item" ng-repeat="step in steps" ng-class="{active: $last}" ng-switch="$last || !!step.abstract"><a ng-switch-when="false" href="{{step.ncyBreadcrumbLink}}">{{step.ncyBreadcrumbLabel}}</a><span ng-switch-when="true">{{step.ncyBreadcrumbLabel}}</span></li>'
  });

  $stateProvider
  .state('app', {
    abstract: true,
    templateUrl: '../App/views/common/layouts/full.html',
    //page title goes here
    ncyBreadcrumb: {
      label: 'Root',
      skip: true
    },
    resolve: {
      loadCSS: ['$ocLazyLoad', function($ocLazyLoad) {
        // you can lazy load CSS files
        return $ocLazyLoad.load([{
          serie: true,
          name: 'Font Awesome',
          files: ['../../Content/css/font-awesome.min.css']
        },{
          serie: true,
          name: 'Simple Line Icons',
          files: ['../../Content/css/simple-line-icons.css']
        }]);
      }],
      loadPlugin: ['$ocLazyLoad', function ($ocLazyLoad) {
        // you can lazy load files for an existing module
        return $ocLazyLoad.load([{
          serie: true,
          name: 'chart.js',
          files: [
            '../../App/js/libs/Chart.min.js',
            '../../App/js/libs/angular-chart.min.js'
          ]
        }]);
      }],
    }
  })
  .state('app.main', {
    url: '/dashboard',
    templateUrl: '../../App/views/main.html',
    //page title goes here
    ncyBreadcrumb: {
      label: 'Home',
    },
    //page subtitle goes here
    params: { subtitle: 'Welcome to Genius Bootstrap 4 Admin Template' },
    resolve: {
      loadPlugin: ['$ocLazyLoad', function ($ocLazyLoad) {
        // you can lazy load files for an existing module
        return $ocLazyLoad.load([
          {
            serie: true,
            name: 'chart.js',
            files: [
              '../../App/js/libs/Chart.min.js',
              '../../App/js/libs/angular-chart.min.js'
            ]
          },
          {
            serie: true,
            files: ['../../App/js/libs/moment.min.js']
          },
          {
            serie: true,
            files: ['../../App/js/libs/gauge.min.js']
          },
          {
            serie: true,
            files: ['../../App/js/libs/angular-gauge.js']
          },
          {
            serie: true,
            files: [
              '../../App/js/libs/daterangepicker.js',
              '../../App/js/libs/angular-daterangepicker.min.js'
            ]
          },
          {
              files: ['../../App/js/libs/angular-toastr.tpls.min.js']
          }
        ]);
      }],
      loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
        // you can lazy load controllers
        return $ocLazyLoad.load({
            files: ['../App/js/controllers/main.js']
        });
      }]
    }
      })

    //mis rutas
    //cond�minos
  $stateProvider
      .state('app.condominos', {
          url: '/condominos',
          templateUrl: '../App/condominos/condominos.html',
          controller: 'CondominoCtrl',
          ncyBreadcrumb: {
              label: 'Condominos',
          },
          resolve: {
              loadPlugin: ['$ocLazyLoad', function ($ocLazyLoad) { }]
                  }
      })

  $stateProvider
      .state('app.addcondos', {
          url: '/condominos/agregar',
          templateUrl: '../App/condominos/agregar.html',
          controller: 'AddCondominoCtrl',
          ncyBreadcrumb: {
              label: 'Agregar  Cond�mino',
          },
          resolve: {
              loadPlugin: ['$ocLazyLoad', function ($ocLazyLoad) { }]
          }
      })

  $stateProvider
      .state('app.detcondos', {
          url: '/condominos/detalles',
          templateUrl: '../App/condominos/detalle.html',
          controller: 'DetCondominoCtrl',
          ncyBreadcrumb: {
              label: 'Detalles Cond�mino',
          },
          resolve: {
              loadPlugin: ['$ocLazyLoad', function ($ocLazyLoad) { }]
          }
      })

    //ingresos
  $stateProvider
      .state('app.ingresos', {
          url: '/ingresos',
          templateUrl: '../App/ingresos/ingresos.html',
          controller: 'IngresosCtrl',
          ncyBreadcrumb: {
              label: 'Ingresos',
          },
          resolve: {
              loadPlugin: ['$ocLazyLoad', function ($ocLazyLoad) { }]
          }
      })

  $stateProvider
      .state('app.addingresos', {
          url: '/ingresos',
          templateUrl: '../App/ingresos/agregar.html',
          controller: 'AddIngresosCtrl',
          ncyBreadcrumb: {
              label: 'Agregar Ingresos',
          },
          resolve: {
              loadPlugin: ['$ocLazyLoad', function ($ocLazyLoad) { }]
          }
      })

  //egresos
  $stateProvider
    .state('app.egresos', {
          url: '/egresos',
          templateUrl: '../App/egresos/egresos.html',
          controller: 'EgresosCtrl',
          ncyBreadcrumb: {
              label: 'Egresos'
      }
      })
  $stateProvider
      .state('app.addegresos', {
          url: '/egresos/agregar',
          templateUrl: '../App/egresos/agregar.html',
          controller: 'AddEgresosCtrl',
          ncyBreadcrumb: {
              label: 'Agregar Egresos'
          }
      })

  $stateProvider
      .state('app.detegresos', {
          url: '/egresos/detalle',
          templateUrl: '../App/egresos/detalles.html',
          controller: 'DetEgresosCtrl',
          ncyBreadcrumb: {
              label: 'Detalle Egresos'
          }
      })

      //proveedores
  $stateProvider
      .state('app.proveedores', {
          url: '/proveedores',
          templateUrl: '../App/proveedores/proveedores.html',
          controller: 'ProveedoresCtrl',
          ncyBreadcrumb: {
              label: 'Proveedores'
          }
      })

  $stateProvider
      .state('app.addproveedor', {
          url: '/proveedores/agregar',
          templateUrl: '../App/proveedores/agregar.html',
          controller: 'AddProveedoresCtrl',
          ncyBreadcrumb: {
              label: 'Agregar Proveedor'
          }
      })

  $stateProvider
      .state('app.detproveedor', {
          url: '/proveedores/detalle',
          templateUrl: '../App/proveedores/detalle.html',
          controller: 'DetProveedoresCtrl',
          ncyBreadcrumb: {
              label: 'Detalle Proveedor'
          }
      })

      //configuracion
  $stateProvider
      .state('app.configuracion', {
          url: '/configuracion',
          templateUrl: '../App/configuracion/configuracion.html',
          controller: 'ConfiguracionCtrl',
          ncyBreadcrumb: {
              label: 'Configuracion'
          }
      })

  $stateProvider
      .state('app.conceptos', {
          url: '/configuracion/conceptos',
          templateUrl: '../App/configuracion/conceptos.html',
          controller: 'ConceptosCtrl',
          ncyBreadcrumb: {
              label: 'Conceptos de Cobro'
          }
      })

  $stateProvider
      .state('app.propiedades', {
          url: '/configuracion/propiedades',
          templateUrl: '../App/configuracion/propiedades.html',
          controller: 'PropiedadesCtrl',
          ncyBreadcrumb: {
              label: 'Propiedades del Buz�n'
          }
      })

      //pass
  $stateProvider
      .state('app.perfil', {
          url: '/perfil/cambiar',
          templateUrl: '../App/perfil/perfil.html',
          controller: 'PerfilCtrl',
          ncyBreadcrumb: {
              label: 'Perfil'
          }
      })

  //UI Kits - Email App
  .state('app.uikits.email', {
    abstract: true,
    template: '<ui-view></ui-view>',
    ncyBreadcrumb: {
      label: 'Email'
    }
  })
  .state('app.uikits.email.inbox', {
    url: '/uikits/email/inbox',
    templateUrl: 'views/UIkits/email/inbox.html',
    ncyBreadcrumb: {
      label: '{{ "INBOX" | translate }}'
    }
  })
  .state('app.uikits.email.message', {
    url: '/uikits/email/message',
    templateUrl: 'views/UIkits/email/message.html',
    ncyBreadcrumb: {
      label: '{{ "INBOX" | translate }}'
    }
  })
  .state('app.uikits.email.compose', {
    url: '/uikits/email/compose',
    templateUrl: 'views/UIkits/email/compose.html',
    ncyBreadcrumb: {
      label: '{{ "INBOX" | translate }}'
    },
    resolve: {
      loadPlugin: ['$ocLazyLoad', function ($ocLazyLoad) {
        // you can lazy load files for an existing module
        return $ocLazyLoad.load([
          {
            files: ['js/libs/select.min.js']
          }
        ]);
      }],
      loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
        // you can lazy load controllers
        return $ocLazyLoad.load({
          files: ['js/controllers/uikits/email.js']
        });
      }]
    }
      })

    //mis rutas

}]);

using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace Mues.Managers
{
    public class MongoConnection
    {
        string connectionString = @"mongodb://harriroot:p94g91m1c34n@cluster0-shard-00-00-b69uk.mongodb.net:27017,cluster0-shard-00-01-b69uk.mongodb.net:27017,cluster0-shard-00-02-b69uk.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin";

        protected static IMongoClient mongoClient;
        protected static IMongoDatabase db;


        public MongoConnection(string dbName)
        {
            MongoClientSettings settings = MongoClientSettings.FromUrl(
                new MongoUrl(connectionString)
           );

            settings.SslSettings =
                new SslSettings() { EnabledSslProtocols = SslProtocols.Tls12 };
            mongoClient = new MongoClient(settings);
            db = mongoClient.GetDatabase(dbName);
        }

        public IMongoDatabase GetDatabase()
        {
            return db;
        }

        public void Insert(string nameCollection, object document)
        {
            BsonDocument newDocument = BsonDocument.Parse(JsonConvert.SerializeObject(document));
            newDocument.Remove("id");
            db.GetCollection<BsonDocument>(nameCollection).InsertOne(newDocument);
        }

        public string GetCollection(string nameCollection)
        {
            return db.GetCollection<BsonDocument>(nameCollection).Find(x => true).ToList().ToJson();
        }

        public string GetCollection(string nameCollection, string key, string value)
        {
            var filter = Builders<BsonDocument>.Filter.Regex(key, value);
            return db.GetCollection<BsonDocument>(nameCollection).Find(filter).ToList().ToJson();
        }

        public string GetDocument(string nameCollection, string id)
        {
            var objectId = ObjectId.Parse(id);
            var collection = db.GetCollection<BsonDocument>(nameCollection);
            var filter = Builders<BsonDocument>.Filter.Eq("_id", objectId);

            var result = collection.Find(filter).FirstOrDefault();
            if (result != null)
            {
                return result.ToJson();
            }
            else
            {
                return string.Empty;
            }
        }

        public string GetDocument(string nameCollection, string key, string value)
        {
            var collection = db.GetCollection<BsonDocument>(nameCollection);
            var filter = Builders<BsonDocument>.Filter.Regex(key, value);
            
            var result = collection.Find(filter).FirstOrDefault();
            if (result != null)
            {
                return result.ToJson();
            }
            else
            {
                return string.Empty;
            }
        }

      

        public void Update(string nameCollection, object document)
        {
            BsonDocument newDocument = BsonDocument.Parse(JsonConvert.SerializeObject(document));
            string idDocument = newDocument.GetValue("id").ToString();
            var objectId = ObjectId.Parse(idDocument);

            newDocument.Remove("id");

            var collection = db.GetCollection<BsonDocument>(nameCollection);
            var filter = Builders<BsonDocument>.Filter.Eq("_id", objectId);
            collection.FindOneAndReplace(filter, newDocument);
        }

        public void Remove(string nameCollection, string id)
        {
            var objectId = ObjectId.Parse(id);
            
            var collection = db.GetCollection<BsonDocument>(nameCollection);
            var filter = Builders<BsonDocument>.Filter.Eq("_id", objectId);
            collection.DeleteOne(filter);

        }

        //Logear usuario
        public string GetLogin(string email, string password)
        {
            //  {Encriptacion segura con sha3 }
            byte[] datos = ASCIIEncoding.UTF8.GetBytes(password);
            byte[] resultadoHash;
            SHA384 sha3 = new SHA384Managed();
            resultadoHash = sha3.ComputeHash(datos);
            password = Convert.ToBase64String(resultadoHash);
            //  {/ Encriptacion segura con sha3 }

            var collection = db.GetCollection<BsonDocument>("usuarios");
            var filter = Builders<BsonDocument>.Filter.Regex("Email", email) & 
                Builders<BsonDocument>.Filter.Regex("Password", password);
            var result = collection.Find(filter).FirstOrDefault();
            if (result != null)
            {
                return result.ToJson();
            }
            else
            {
                return string.Empty;
            }
        }

    }
}